[![pipeline status](https://gitlab.com/appf-anu/python-project-template/badges/master/pipeline.svg)](https://gitlab.com/appf-anu/python-project-template/-/commits/master)

# python-project-template

See documentation at [readthedocs](https://python-project-template.readthedocs.io/en/latest/).
