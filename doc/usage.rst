Usage
=====

..
   code block example
   - must have blank line after: .. code-block:: bash

.. code-block:: bash

   projname --param1 2 \
       --param2 3 \
       --output output.csv

- ``param1`` explanation here
- ``param2`` explanation here
- ``-o/--output`` for the output filename (can be CSV or XLSX)

