.. 
   rename this file to match actual section name
   Heading here should match heading in index.rst TOC

Section name
==============================================

Documentation text goes here.

Bullets and formatting:

* lists (bulleted and numbered) require a blank line to separate from previous
* one asterisk: *text* for emphasis (italics),
* two asterisks: **text** for strong emphasis (boldface), and
* backquotes: ``text`` for code samples.

Numbered list:

#. first item
#. second item

Example link: `example <https:example.com>`_


A table

+---+---+---+---+
| A | B | C | D |
+===+===+===+===+
| 1 | 2 | 3 | 4 |
+---+---+---+---+
| 5 | 6 | 7 | 8 |
+---+---+---+---+
