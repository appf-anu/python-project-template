..
  file originally autogenerated with:
  sphinx-apidoc -o doc projname

projname package
================

Submodules
----------

projname.projname module
------------------------

.. automodule:: projname.projname
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: projname
   :members:
   :undoc-members:
   :show-inheritance:
