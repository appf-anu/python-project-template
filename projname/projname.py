# Copyright (c) Your name

# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, you can
# obtain one at http://mozilla.org/MPL/2.0/.

# includes sphinx docstring template

import sys

def myfunc():
	"""Summary goes here of this function importable function

	:param [ParamName]: [ParamDescription], defaults to [DefaultParamVal]
	:type [ParamName]: [ParamType](, optional)
	:raises [ErrorType]: [ErrorDescription]
	:return: [ReturnDescription]
	:rtype: [ReturnType]
	"""

	# some trivial code
	return sys.argv[1] + sys.argv[2]

if __name__ == '__main__':
	myfunc()
	